Takes all tables in `out/tables/` and deduplicates any duplicated columns by adding an index. All other tables 
that do not contain duplicated headers are moved to out unmodified.