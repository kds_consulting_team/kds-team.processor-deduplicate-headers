import json
import logging
import os
import shutil
from csv import DictReader
from pathlib import Path

from keboola.component import ComponentBase, UserException
from keboola.component.dao import TableDefinition
# configuration variables
from keboola.utils.header_normalizer import DefaultHeaderNormalizer

# global constants'

KEY_INDEX_SEPARATOR = 'index_separator'
# #### Keep for debug
KEY_DEBUG = 'debug'
MANDATORY_PARS = []


class KBCHeaderNormalizer(DefaultHeaderNormalizer):

    def _normalize_column_name(self, header: str) -> str:
        normalized = super()._normalize_column_name(header)
        permitted_characters = self.permitted_chars.replace('_', '')

        if normalized[0] not in permitted_characters:
            normalized = normalized[1:]

        if normalized[-1] not in permitted_characters:
            normalized = normalized[:-1]

        return normalized


class Component(ComponentBase):

    def __init__(self):
        ComponentBase.__init__(self)
        self.validate_configuration_parameters(MANDATORY_PARS)

    def run(self):
        '''
        Main execution code
        '''
        params = self.configuration.parameters
        tables = self.get_input_tables_definitions()
        for t in tables:
            self.dedupe_headers(t, params.get(KEY_INDEX_SEPARATOR, '_'))

    def dedupe_headers(self, t: TableDefinition, index_separator='_'):
        header = self.get_header(t)
        new_header = list()
        new_header_lower = list()
        dup_cols = dict()
        for c in header:
            if c.lower() in new_header_lower:
                new_index = dup_cols.get(c.lower(), 0) + 1
                new_col_name = c + index_separator + str(new_index)
                new_header.append(new_col_name)
                new_header_lower.append(new_col_name.lower())
                dup_cols[c.lower()] = new_index
            else:
                new_header.append(c)
                new_header_lower.append(c.lower())

        if not dup_cols:
            # just move the files
            self._copy_table_to_out(t)

            t.full_path = t.full_path.replace(self.tables_in_path, self.tables_out_path)
            self.write_manifest(t)

            return
        # else replace
        logging.warning(f'Table {t.name} contains duplicate headers, deduplicating..')

        if t.is_sliced:
            self.add_columns_to_definition(t, new_header)
            shutil.copytree(t.full_path, t.full_path.replace(self.tables_in_path, self.tables_out_path))

        elif t.column_names:
            self.add_columns_to_definition(t, new_header)
            shutil.copy(t.full_path, Path(self.tables_out_path))
        else:
            self.replace_header_in_file_and_move(t.full_path, new_header, t.delimiter)
            self.add_columns_to_definition(t, new_header)

        t.full_path = t.full_path.replace(self.tables_in_path, self.tables_out_path)
        self.write_manifest(t)

    def get_header(self, t: TableDefinition):

        if t.is_sliced or t.column_names:
            with open(t.full_path + ".manifest") as file:
                manifes = json.load(file)
                if manifes.get("columns"):
                    header = manifes.get("columns")
                else:
                    header = t.column_names

                return header

        else:
            with open(t.full_path) as input:
                delimiter = t.delimiter
                enclosure = t.enclosure
                reader = DictReader(input, lineterminator='\n', delimiter=delimiter, quotechar=enclosure)
                header = reader.fieldnames
        normalizer = KBCHeaderNormalizer()
        header_norm = normalizer.normalize_header(header)

        return header_norm

    def replace_header_in_file_and_move(self, f, new_header, separator=','):
        new_path = os.path.join(self.tables_out_path, Path(f).name)
        with open(f) as from_file, open(new_path, mode="w") as to_file:
            line = from_file.readline()

            line = separator.join(new_header)
            to_file.write(line + '\n')
            # the pointer in original file is 1 now
            shutil.copyfileobj(from_file, to_file)

    def add_columns_to_definition(self, table: TableDefinition, new_column_names: list):
        table.delete_columns(table.column_names)
        table.add_columns(new_column_names)

    def _copy_table_to_out(self, t: TableDefinition):
        if Path(t.full_path).is_dir():
            shutil.copytree(t.full_path, t.full_path.replace(self.tables_in_path, self.tables_out_path))
        else:
            shutil.copy(t.full_path, t.full_path.replace(self.tables_in_path, self.tables_out_path))


"""
        Main entrypoint
"""
if __name__ == "__main__":
    try:
        comp = Component()
        # this triggers the run method by default and is controlled by the configuration.action parameter
        comp.execute_action()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
